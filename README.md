# Aplicación Microservicio
_Este proyecto es una aplicación de tipo Microservicio para el curso Arquitecto DevOps - mitocode._


## Objetivo
_Este proyecto tiene como finalidad aplicar lo aprendido en el curso, mediante la implementación de Integracón continua, Entrega continua y Despliegue Continuo_


## Pre-requisitos
_Se requerie acceso a:_

* [Jenkins](http://208.68.39.62:8080/)
* [Sonarqube](http://208.68.39.62:9000/projects)

_Repositorio Público_

* [Bitbucket](https://bitbucket.org/curso-mitocodews/api-covid19)


## Ejecutar pipeline
_Para poder ejecutar el job, se debe ingresar al servidor de_ [Jenkins](http://208.68.39.62:8080/)_, luego al workspace de curso-mitocodews, repositorio
api-covid19 y selecionar la rama que corresponda y por último dar clic en la opción construir ahora._

_Fases:_

* Build
* Testing
* Sonar  
* Despliegue en Dockerhub


## Sonarqube
_Para visualizar el reporte, se debe ingresar a_ [Sonarqube](http://208.68.39.62:9000/dashboard?id=com.glam%3Aapi-covid19)

_También se puede observar la cobertura  en jenkins en la sección tendencia de cobertura._


## Docker Hub 
_Si se requiere validar el artefacto desplegado se debe de ingresar a: [Docker Hub](https://hub.docker.com/r/gilmarlam/api-covid19/tags)_


## Herramientas utilizadas

* [Bitbucket](https://bitbucket.org/curso-mitocodews/app-monolith-covid19) - Repositorio de código fuente
* [Jenkins](http://208.68.39.62:8080/) - Servidor de automatización
* [Sonarqube](http://208.68.39.62:9000/projects) - Herramienta para evaluar código fuente
* [Docker Hub](https://hub.docker.com/r/gilmarlam) - Repositorio público en la nube
* [Maven](https://maven.apache.org/) - Herramienta de software para la gestión y construcción de proyectos
* [Docker](https://www.docker.com/) - Automatiza el despliegue de aplicaciones dentro de contenedores de software
* [JaCoCo](https://www.jacoco.org/jacoco/trunk/doc/) - Plugin de maven que se utiliza para realizar reportes
* [Git](https://git-scm.com/) - Software de control de versiones
* [Junit4](https://junit.org/junit4/) - herramienta para pruebas unitarias


## Autor
* **Gilmar Lam** - *Alumno* - [gilmarlam](https://bitbucket.org/gilmarlam/) 
 
 
