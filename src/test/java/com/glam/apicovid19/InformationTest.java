package com.glam.apicovid19;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.glam.apicovid19.model.Informacion;
import com.glam.apicovid19.model.Pais;
import com.glam.apicovid19.repository.InformacionRepository; 
import com.glam.apicovid19.service.InformacionServiceImpl;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.*;



@RunWith(MockitoJUnitRunner.class)
public class InformationTest {

	private final static Logger log = Logger.getLogger(InformationTest.class);
	
	@InjectMocks
	private InformacionServiceImpl informacionServiceImpl;
	
	@Mock
	InformacionRepository informacionRepository;
	
	@Test
	public void findByPais()   {
		List<Informacion> lista = buildListInformacion();
		when(informacionRepository.findByPais(any())).thenReturn(lista);
		List<Informacion> list = informacionServiceImpl.findByPais(any());
		log.info("testFindAll: list:"+list.size());
		assertTrue(!list.isEmpty());
		assertThat(list.get(0).getCasosConfirmados(),notNullValue());
		assertThat(list.get(0).getCasosNegativos(),notNullValue());
		assertThat(list.get(0).getFallecidos(),notNullValue());
		assertThat(list.get(0).getFecha(),notNullValue());
		assertThat(list.get(0).getHospitalizados(),notNullValue());
		assertThat(list.get(0).getId(),notNullValue());
		assertThat(list.get(0).getIdPais(),notNullValue());
		assertThat(list.get(0).getPersonasDadasAlta(),notNullValue());
		assertThat(list.get(0).getPersonasMuestreadas(),notNullValue());
		
		 
	}
	 	
	
	private List<Informacion> buildListInformacion() {		
		List<Informacion> lista = new ArrayList<>();
		lista.add(buildInformacion());
		return lista;
	}
	
	private Informacion buildInformacion() {	
		Random aleatorio = new Random(System.currentTimeMillis()); 
		Informacion info = new Informacion();
		info.setId(aleatorio.nextInt(1000)+1);
		info.setCasosConfirmados(aleatorio.nextInt(10000)+1);
		info.setCasosNegativos(aleatorio.nextInt(10000)+1);
		info.setFallecidos(aleatorio.nextInt(10000)+1);
		info.setFecha(new Date());
		info.setHospitalizados(aleatorio.nextInt(10000)+1);
		Pais pais = buildPais();
		info.setIdPais(pais.getId());
		info.setPersonasDadasAlta(aleatorio.nextInt(10000)+1);
		info.setPersonasMuestreadas(aleatorio.nextInt(10000)+1); 
		return info;
	}
	
	private Pais buildPais() {	 
		Pais pais = new Pais();
		pais.setId(1); 
		pais.setNombre("Perú");
		pais.setPoblacion(33000000);
		return pais;
	}
	
}
