package com.glam.apicovid19.service;

import java.util.List;
import java.util.Optional;

import com.glam.apicovid19.model.Informacion;

public interface InformacionService {

	List<Informacion> findByPais(Integer idPais);
}
