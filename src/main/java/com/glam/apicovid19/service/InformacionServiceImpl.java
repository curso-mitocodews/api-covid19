package com.glam.apicovid19.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.glam.apicovid19.model.Informacion;
import com.glam.apicovid19.repository.InformacionRepository;


@Service
public class InformacionServiceImpl implements InformacionService{

	@Autowired
	private InformacionRepository informacionRepository;
	
	
	@Override
	public List<Informacion> findByPais(Integer idPais) { 
		return informacionRepository.findByPais(idPais);
	}

}
