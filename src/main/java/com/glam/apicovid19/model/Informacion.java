package com.glam.apicovid19.model;

 

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Informacion {

	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	private Integer idPais;
	private Date fecha;
	private Integer casosConfirmados;
	private Integer personasMuestreadas;
	private Integer casosNegativos;
	private Integer hospitalizados;
	private Integer personasDadasAlta;
	private Integer fallecidos;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getIdPais() {
		return idPais;
	}
	public void setIdPais(Integer idPais) {
		this.idPais = idPais;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public Integer getCasosConfirmados() {
		return casosConfirmados;
	}
	public void setCasosConfirmados(Integer casosConfirmados) {
		this.casosConfirmados = casosConfirmados;
	}
	public Integer getPersonasMuestreadas() {
		return personasMuestreadas;
	}
	public void setPersonasMuestreadas(Integer personasMuestreadas) {
		this.personasMuestreadas = personasMuestreadas;
	}
	public Integer getCasosNegativos() {
		return casosNegativos;
	}
	public void setCasosNegativos(Integer casosNegativos) {
		this.casosNegativos = casosNegativos;
	}
	public Integer getHospitalizados() {
		return hospitalizados;
	}
	public void setHospitalizados(Integer hospitalizados) {
		this.hospitalizados = hospitalizados;
	}
	public Integer getPersonasDadasAlta() {
		return personasDadasAlta;
	}
	public void setPersonasDadasAlta(Integer personasDadasAlta) {
		this.personasDadasAlta = personasDadasAlta;
	}
	public Integer getFallecidos() {
		return fallecidos;
	}
	public void setFallecidos(Integer fallecidos) {
		this.fallecidos = fallecidos;
	}
	public Informacion() { 
	}
	
	
	
}
