package com.glam.apicovid19.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.glam.apicovid19.model.Informacion;
import com.glam.apicovid19.service.InformacionService;

@RestController
@RequestMapping(value = "/info", produces = MediaType.APPLICATION_JSON_VALUE)
public class InformacionController {

	@Autowired
	private InformacionService informacionService;

	
	@GetMapping("/pais/{id}") 
	public ResponseEntity<List<Informacion>> findByPais(@PathVariable Integer id) {
		List<Informacion> list = informacionService.findByPais(id);
	   return ResponseEntity.ok(list); 

	}
	
	

}
