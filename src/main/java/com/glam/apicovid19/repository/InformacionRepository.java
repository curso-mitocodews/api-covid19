package com.glam.apicovid19.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.glam.apicovid19.model.Informacion;
 
 
@Repository
public interface InformacionRepository extends JpaRepository<Informacion,Integer> {
	@Query("SELECT d FROM Informacion d where idPais=:idPais")
	List<Informacion> findByPais(@Param("idPais") Integer idPais);
 
}
