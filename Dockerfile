FROM openjdk:11.0.6-jdk
LABEL usuario="Gilmar"
WORKDIR /workspace
COPY target/api-*.jar app.jar
EXPOSE 9090
ENTRYPOINT exec java -jar /workspace/app.jar
